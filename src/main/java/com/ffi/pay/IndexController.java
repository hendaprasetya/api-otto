/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ffi.pay;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author henda
 */
@RestController
public class IndexController {

    @Value("${server.api}")
    private String propUrl;

    @RequestMapping(value = "/post.action", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Contoh Response list yang di Tampilkan", response = Object.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK"),
        @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public @ResponseBody
    Map<String, Object> post(@RequestBody String param) {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            Gson gson = new Gson();
            Map<String, Object> parameter = gson.fromJson(param, new TypeToken<Map<String, Object>>() {
            }.getType());
            String uri = propUrl.concat("/").concat(parameter.get("uri").toString());

            HttpClient httpClient = HttpClientBuilder.create().build();
            HttpPost post = new HttpPost(uri);
            String prm = gson.toJson(parameter.get("param"));
            System.out.println(uri + " PARAM = " + prm);
            StringEntity postingString = new StringEntity(prm);//gson.tojson() converts your pojo to json
            post.setEntity(postingString);
            post.setHeader("Content-type", "application/json");
            post.setHeader("Liquid-Api-Key", parameter.get("key").toString());
            HttpResponse response = httpClient.execute(post);
            BufferedReader br = new BufferedReader(
                    new InputStreamReader(
                            (response.getEntity().getContent())
                    )
            );

            StringBuilder content = new StringBuilder();
            String line;
            while (null != (line = br.readLine())) {
                content.append(line);
            }
            String result = content.toString();
            map = gson.fromJson(result, new TypeToken<Map<String, Object>>() {
            }.getType());

        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(IndexController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(IndexController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return map;
    }

    @RequestMapping(value = "/confirm.action", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Contoh Response list yang di Tampilkan", response = Object.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK"),
        @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public @ResponseBody
    Map<String, Object> confirm(String param) {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            Gson gson = new Gson();
            Map<String, Object> parameter = gson.fromJson(param, new TypeToken<Map<String, Object>>() {
            }.getType());
            String uri = propUrl.concat("/").concat(parameter.get("uri").toString());
            HttpClient httpClient = HttpClientBuilder.create().build();

            HttpGet get = new HttpGet(uri);
            get.setHeader("Content-type", "application/json");
            get.setHeader("Liquid-Api-Key", parameter.get("key").toString());
            HttpResponse resp = httpClient.execute(get);
            BufferedReader br2 = new BufferedReader(
                    new InputStreamReader(
                            (resp.getEntity().getContent())
                    )
            );

            StringBuilder cont = new StringBuilder();
            String ln;
            while (null != (ln = br2.readLine())) {
                cont.append(ln);
            }
            String rslt = cont.toString();
            map = gson.fromJson(rslt, new TypeToken<Map<String, Object>>() {
            }.getType());

        } catch (IOException ex) {
            Logger.getLogger(IndexController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return map;
    }

    @RequestMapping(value = "/refund.action", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Contoh Response list yang di Tampilkan", response = Object.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK"),
        @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public @ResponseBody
    Map<String, Object> refund(@RequestBody String param) {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            Gson gson = new Gson();
            Map<String, Object> parameter = gson.fromJson(param, new TypeToken<Map<String, Object>>() {
            }.getType());
            String uri = propUrl.concat("/").concat(parameter.get("uri").toString());
            System.out.println("URI = " + uri);
            HttpClient httpClient = HttpClientBuilder.create().build();
            HttpPost post = new HttpPost(uri);
            String prm = gson.toJson(parameter.get("param"));
            post.setHeader("Content-type", "application/json");
            post.setHeader("Liquid-Api-Key", parameter.get("key").toString());
            StringEntity postingString = new StringEntity(prm);//gson.tojson() converts your pojo to json
            post.setEntity(postingString);
            HttpResponse resp = httpClient.execute(post);
            BufferedReader br2 = new BufferedReader(
                    new InputStreamReader(
                            (resp.getEntity().getContent())
                    )
            );

            StringBuilder cont = new StringBuilder();
            String ln;
            while (null != (ln = br2.readLine())) {
                cont.append(ln);
            }
            String rslt = cont.toString();
            map = gson.fromJson(rslt, new TypeToken<Map<String, Object>>() {
            }.getType());

        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(IndexController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(IndexController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return map;
    }

    @RequestMapping(value = "/cust-valid.action", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Contoh Response list yang di Tampilkan", response = Object.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK"),
        @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public @ResponseBody
    Map<String, Object> custValid(@RequestBody String param) {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            Gson gson = new Gson();
            Map<String, Object> parameter = gson.fromJson(param, new TypeToken<Map<String, Object>>() {
            }.getType());
            String uri = propUrl.concat("/").concat(parameter.get("uri").toString());
            HttpClient httpClient = HttpClientBuilder.create().build();
            System.out.println("URI = " + uri);
            HttpGet get = new HttpGet(uri);
            get.setHeader("Content-type", "application/json");
            get.setHeader("Liquid-Api-Key", parameter.get("key").toString());
            HttpResponse resp = httpClient.execute(get);
            BufferedReader br2 = new BufferedReader(
                    new InputStreamReader(
                            (resp.getEntity().getContent())
                    )
            );

            StringBuilder cont = new StringBuilder();
            String ln;
            while (null != (ln = br2.readLine())) {
                cont.append(ln);
            }
            String rslt = cont.toString();
            map = gson.fromJson(rslt, new TypeToken<Map<String, Object>>() {
            }.getType());

        } catch (IOException ex) {
            Logger.getLogger(IndexController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return map;
    }
    
    @RequestMapping(value = "/tes", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    @ApiOperation(value = "Contoh Response list yang di Tampilkan", response = Object.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK"),
        @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public String tes(){
        return "OKE";
    }
}
